export default {
  namespaced: true,
  state: {
    country: null,  // string, current clicked country
    countryContent: null,  // clicked country details json, see below example
    cities: null, // json formatted list of cities added to map display
    city: null,  // string, clicked selected city
    cityContent: null,  // clicked city details json format, see below example
    apiKey: '88ae4b5b-43dc-45a3-a887-00c1abd92d8e'  // for graphQL requests
  },
  mutations: {
    switchSidebar(state, value) {
      if (value) {
        state.sidebarClose = value;
      } else {
        state.sidebarClose = !state.sidebarClose;
      }
    },
    updateCountry(state, country) {
      // console.log("Vuex updateCountry");
      state.country = country;
    },
    updateCity(state, city) {
      state.city = city;
    },
    updateCountryContent(state, countryContent) {
      if (countryContent) {   // content example:
        // callingCodes: ["+254"]
        // cities: (20) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
        // currencies: [{…}]
        // languages: (2) [{…}, {…}]
        // name: "Kenya"
        // population: 48468138
        state.countryContent = {
          Population: countryContent.population.toString(),
          Languages: countryContent.languages.length ?
            countryContent.languages.map(e => (e.name)).toString() :
            "Sorry, not available!",
          "Known cities": countryContent.cities.length.toString(),
          "Calling codes": countryContent.callingCodes.toString(),
          Currencies: countryContent.currencies ?
            countryContent.currencies.map(e => (e.name)).toString() : "NA",
          "Vat rate": countryContent.vatRate ? countryContent.vatRate.toString() : "NA",
          "Capital": countryContent.capital ? countryContent.capital.name : "NA",
        }
        state.cities = countryContent.cities.map(city => (
          {
            latitude: city.location ? city.location.lat : -1,
            longitude: city.location ? city.location.long : -1,
            size: 4,
            tooltip: city.name,
          }));
      } else {
        state.countryContent = null;
        state.cities = null;
      }
      // console.log("vuex countryContent", state.countryContent);
    },
    updateCityContent(state, cityContent) {
      if (cityContent) {
        //     {   // content example
        //       "name": "Paris",
        //       "continent": {
        //         "name": "Europe"
        //       },
        //       "population": 2187526,
        //       "geonamesID": 2968815,
        //       "location": {
        //         "lat": 48.856944,
        //         "long": 2.351389
        //       }
        state.cityContent = {
          Continent: cityContent.continent.name,
          Population: cityContent.population.toString(),
          GeonamesID: cityContent.geonamesID.toString(),
          Location: cityContent.location.lat + ", " + cityContent.location.long,
          "There is": "Not much information",
          "On this database": "So I Lorem ipsum",
          "To ": "simulate additional",
          "Very ": "interesting data",
          "I could ": "carry on like this for a while",
          "But ": "I have better to do!",
        }
      } else {
        state.cityContent = null;
      }
      // console.log("vuex cityContent", state.cityContent);
    },
  },
  actions: {
    async getCountryInfo({commit}, countr) {
      const country =
        countr === "United States" ? "United States of America" :
        countr === "China" ? "People's Republic of China" : countr;
      const query = `
        {
        countries(where: {name: {eq: "${country}"}}) {
          name
          population
          vatRate
          callingCodes
          capital {
            name
          }
          currencies {
            name
          }
          languages {
            name
          }
          cities {
            name,
            location {
              lat
              long
            }
          }
          
        }
      }
      `
      const res = await fetch('https://api.everbase.co/graphql?apikey=' + this.apiKey, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({query}),
      })
      let {data} = await res.json()
      const countryContent = data.countries[0]
      // console.log("countryContent", countryContent)
      commit('updateCountry', country);
      commit('updateCountryContent', countryContent)
      // if (!countryContent) {  // handle on mutation and by vue html
      //   console.warn("Warning, fetched did not return data.countries", data.countries)
      // }
    },
    async getCityContent({commit}, {city, countr}) {
      // console.log("getCityContent city, country", city, countr)
      const country =
          countr === "United States" ? "United States of America" :
          countr === "China" ? "People's Republic of China" : countr;
      const query = `
        {
        cities(where: {name: {eq: "${city}"}, countryName: { eq: "${country}" }}) {
          name
          continent {
            name
          }
          population
          geonamesID
          location {
            lat
            long
          }
        }
      }
      `
      const res = await fetch('https://api.everbase.co/graphql?apikey=' + this.apiKey, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({query}),
      })
      let {data} = await res.json()
      const cityContent = data.cities[0]
      // console.log("cityContent", cityContent)
      commit('updateCity', city);
      commit('updateCityContent', cityContent)
    },
  },
};
