import Vue from 'vue';
import Vuex from 'vuex';

import layout from './layout';
import tripTrip from './tripTrip';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    layout,
    tripTrip,
  },
});
