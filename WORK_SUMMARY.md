# Work summary

## Day 1:
* Analysis the challenge, designing a possible solution
* Getting the template from FlatLogic, for its nice look & feel, and its open source Map solution
* Starting updating the map usage
* Managing the zoom when a country is clicked,
* Retrieving the name of the country once clicked
* Creating and generating a graphQL request to get country information
* Implementing it in vueX Actions (to manage asynchronous call)
* Displaying the cities on the map once they are properly fetch
* Managing loading matters countries with numerous cities (China, USA)
* Manage error cases on vueX and on the layout with appropriate messages
* Adding help messages on user interface, so the user understand how it works
* Making & checking the layout is more responsive
* Removing template unused component from the trip layout


## Day 2 

* Trying graphQL Web scratch service, to fetch images and text from wikipedia matching selected city
* Found it not working well and not a reliable source, since the content could be change without warning
* Back to the graphQL service to get city information when one is clicked
* Adding a hook, to save city information on vueX store
* Displaying city content on the layout
* Managing various error cases 
* Adding a dockerfile, but due to new laptop requiring a windows OS update, its finalisation has been postponed (actually this was done few days later)

## Remaining nice-to-have ideas
* Adding a search bar, to look for countries and cities
* Adding city images fetch from external traveling sources
* Adding an animated application gif on the readme to ease its understanding (actually done, its even better since it is live application)
* mapping the world map with a three.js [3D nice planet](https://threejs.org/examples/?q=map#webgl_shaders_tonemapping) view
