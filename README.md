# 🤘 Travel challenge

Thanks to [Flatlogic template](https://flatlogic.com/templates/light-blue-vue-lite/demo) since this application is based on their open-source template.

At the end of the day, few components are still in use, but these components render great user experience.
And it was nice to get some layout ideas from this template.

This application is built to match the challenge describe in [README initial requirement challenge](./README initial requirement challenge.md)

## Here is a demo of the lastest version

[Demo](https://elated-aryabhata-fd80c7.netlify.app/#/app/dashboard)

This deployment is built automatically on latest source code push.
 
## Features

* Bootstrap 4+ & SCSS
* Responsive layout
* amcharts4 to display the Map
* graphQL backend @ [everbase](https://www.everbase.co/)
* Simple login / logout (not activated)
* Error page
* Styled Bootstrap components like buttons, modals, etc

## Works fine using:
* node v12.18.3
* yarn v1.22.4
* npm v6.14.6
* vue cli 3


## Installation for development

1. Clone repository
```shell
git clone git@gitlab.com:loft-orbital-external/coding-challenges/traveler-front-end-challenge.git
```
If required go the branch
```
git checkout thomasL
```
2. Get in the project folder
```shell
cd tripTrip
```
3. Install dependencies via npm or yarn
```shell
npm install
```
or
```shell
yarn
```

## Quick start
Run development server
```shell
yarn run serve
```

## Deployment (production version)
The web app is deployed on Netlify https://app.netlify.com/

May be update to vue cli3
```
 yarn global add @vue/cli
``` 
If you have an account on Netlify, you can configure it to point to your git reposittory and Netlify to build using command 
```
yarn build
```
Once done netfily will deploy this front application.
